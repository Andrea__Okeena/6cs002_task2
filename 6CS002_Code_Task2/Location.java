package base;
import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import base.Location.DIRECTION;

/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class Location extends SpacePlace {
	  public int column;
	  public int row;
	  public DIRECTION direction;
	  int temporaryColumn = column + 1;
	  int temporaryRow = row + 1;
	  public enum DIRECTION {VERTICAL, HORIZONTAL};
	  
	  public Location(int row, int column) {
	    this.row = row;
	    this.column = column;
	  }

	  public Location(int row, int column, DIRECTION direction) {    
	    this(row,column);
	    this.direction = direction;
	  }
	  
	  public String toString() {
	    if(direction==null){
	      return "(" + (temporaryColumn) + "," + (temporaryRow) + ")";
	    } else {
	    	return "(" + (temporaryColumn) + "," + (temporaryRow) + "," + direction + ")";
	    }
	  }
	  
	  public void drawGridLines(Graphics g) {
		    g.setColor(Color.LIGHT_GRAY);
		    drawHorizontalLines(g);
		    drawVerticalLines(g);
		  }

	  private void drawVerticalLines(Graphics g) {
			int numOfLines = 8;
			for (int see = 0; see <= numOfLines; see++) {
		      g.drawLine(20 + see * 20, 20, 20 + see * 20, 160);
		    }
		}

	  private void drawHorizontalLines(Graphics g) {
			int numOfLines = 7;
			for (temporary = 0; temporary <= numOfLines; temporary++) {
		      g.drawLine(20, 20 + temporary * 20, 180, 20 + temporary * 20);
		    }
		}
	  
	  public static int getInt() {
	    BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
	    do {
	      try {
	        return Integer.parseInt(r.readLine());
	      } catch (Exception e) {
	      }
	    } while (true);
	  }
	}
